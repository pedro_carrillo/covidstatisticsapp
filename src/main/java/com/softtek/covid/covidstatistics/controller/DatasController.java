package com.softtek.covid.covidstatistics.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.covid.covidstatistics.CovidDatasApplication;
import com.softtek.covid.covidstatistics.model.entity.Area;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "${cors.front.uri}", maxAge = 3600)
@RestController
@EnableScheduling
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "50" ),
                @HystrixProperty( name = "maxQueueSize", value = "25") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "25"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000")})
public class DatasController implements CommandLineRunner {

    private final ObjectMapper objectMapper;

    private ApplicationHome home = new ApplicationHome(CovidDatasApplication.class);

    @Autowired
    public DatasController(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    @GetMapping(path = "/datas",
                produces = "application/json;**charset=UTF-8**")
    @HystrixCommand(threadPoolKey = "getDatasThreadPool")
    public ResponseEntity<String> getDatas(@RequestParam(required = false) String country,
                                           @RequestParam(required = false) String state){
        List<Area> areasList = new ArrayList();

        Area response;

        try{
            File file = new File(home.getDir() + "\\globalDatas.json");
            response = objectMapper.readValue(file,Area.class);
        }catch(Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        areasList.add(response);

        fillIncrements(areasList);

        areasList = getSelectedCountry(areasList,country); //Calls a method to filter the list if a country was received.

        if(areasList.size()==1)
            areasList = getSelectedState(areasList,country,state); //Calls a method to filter the list if a country and a state
                                                                // was received.
        try{
            String arrToJson = objectMapper.writeValueAsString(areasList);
            if(arrToJson.equals("[]"))
                return new ResponseEntity<>(arrToJson, HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(arrToJson, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/covid")
    @Scheduled(fixedRate = 900000)
    public ResponseEntity<String> getCovidData(){

        ResponseEntity<String> responseEntity;

        responseEntity = getDataHTML();

        if(responseEntity.getStatusCode() == HttpStatus.OK)
            responseEntity = getGlobalData();

        return responseEntity;
    }

    public void fillIncrements(List<Area> areasList){
        Area tempArea = areasList.get(0);
        Area tempCountry;
        Area tempState;

        List<Area> tempCountryList = tempArea.getAreas();
        List<Area> tempStateList;

        tempArea.setPercentages();

        for (Area country:tempCountryList) {
            tempCountry = country;
            tempCountry.setPercentages();
            tempStateList = tempCountry.getAreas();
            for (Area state:tempStateList) {
                tempState = state;
                tempState.setPercentages();
            }
        }
    }

    public ResponseEntity<String> getDataHTML(){
        ResponseEntity<String> responseEntity = new ResponseEntity(HttpStatus.OK);
        try {
            URL url = new URL("https://www.bing.com/covid/local");
            File file = new File(home.getDir() + "\\htmlDatas.txt");
            FileUtils.copyURLToFile(url, file);
        } catch (IOException e) {
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
            return responseEntity;
        }
        return responseEntity;
    }

    public ResponseEntity<String> getGlobalData(){
        ResponseEntity<String> responseEntity = new ResponseEntity(HttpStatus.OK);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(home.getDir() + "\\globalDatas.json"));){
            String text = Files.toString(new File(home.getDir() + "\\htmlDatas.txt"), StandardCharsets.UTF_8);
            text = text.split("var data=")[1];
            text = text.split(";</script>")[0];
            writer.write(text);
        }catch (IOException e){
            responseEntity = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            return responseEntity;
        }
        return responseEntity;
    }

    public List<Area> getSelectedCountry(List<Area> areas, String country){
        List<Area> areasList = areas;
        if(country!=null){
            areasList = areasList.get(0).getAreas();
            areasList = areasList.stream().filter(x->x.getDisplayName().equals(country)).collect(Collectors.toList());
        }
        return areasList;
    }

    public List<Area> getSelectedState(List<Area> areas, String country, String state){
        List<Area> areasList = areas;
        if(state!=null&&country!=null){
            Area selectedCountry = areasList.get(0);

            List<Area> selectedCountryAreas = new ArrayList();


            for (int i = 0; i < selectedCountry.getAreas().size(); i++) {
                if (selectedCountry.getAreas().get(i).getDisplayName().equals(state)) {
                    selectedCountryAreas.add(selectedCountry.getAreas().get(i));
                }
            }
            areasList = selectedCountryAreas.stream().collect(Collectors.toList());
        }
        return areasList;
    }

    @Override
    public void run(String... args) throws Exception {
        getCovidData();
    }
}
