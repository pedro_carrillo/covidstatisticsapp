package com.softtek.covid.covidstatistics.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.logging.Logger;

@Component
@Aspect
public class Aop {

    private String logString;

    private static final Logger LOG = Logger.getLogger(Aop.class.getName());

    @Before("execution(* com.softtek.covid.covidstatistics.controller.DatasController.getDatas(..))")
    public void logBefore(JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        logString = "HTTP_METHOD: " + request.getMethod()
                + ", URL: " + request.getRequestURL().toString()
                + ", Arguments: " + Arrays.toString(joinPoint.getArgs());
    }

    @AfterReturning(value = "execution(* com.softtek.covid.covidstatistics.controller.DatasController.getDatas(..))", returning = "result")
    public void logAfter(Object result){
        logString += ", HTTP_STATUS: "+this.getValue(result);
        LOG.info(logString);
    }

    @AfterReturning(value = "execution(* com.softtek.covid.covidstatistics.controller.DatasController.getCovidData(..))", returning = "result")
    public void logAfterGetData(ResponseEntity result){
        LOG.info("URL: http://localhost:8070/covid, Get Covid Data - HTTP_STATUS: " + result.getStatusCode());
    }

    private HttpStatus getValue(Object result){
        ResponseEntity response = (ResponseEntity) result;
        return response.getStatusCode();
    }
}
