package com.softtek.covid.covidstatistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableCircuitBreaker
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class CovidDatasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidDatasApplication.class);
	}

}
