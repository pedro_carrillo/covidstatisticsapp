package com.softtek.covid.covidstatistics.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;
import java.util.List;

public class Area {
    private String id;

    private String displayName;

    private List<Area> areas;

    @JsonProperty("totalConfirmed")
    private int totalConfirmed;

    @JsonProperty("totalDeaths")
    private int totalDeaths;

    @JsonProperty("totalRecovered")
    private int totalRecovered;

    @JsonProperty("totalRecoveredDelta")
    private int totalRecoveredDelta;

    @JsonProperty("totalDeathsDelta")
    private int totalDeathsDelta;

    @JsonProperty("totalConfirmedDelta")
    private int totalConfirmedDelta;

    @JsonProperty("lastUpdated")
    private Date lastUpdated;

    private double lat;

    @JsonProperty("long")
    private double lon;

    private String parentId;

    private float mortalityRate;

    private float mortalityIncrementPercentage;

    private float confirmedIncrementPercentage;

    private float recoveredRate;

    private float recoveredIncrementPercentage;

    public Area() {
    }

    public Area(String id, String displayName, List<Area> areas, int totalConfirmed, int totalDeaths,
                int totalRecovered, int totalRecoveredDelta, int totalDeathsDelta, int totalConfirmedDelta,
                Date lastUpdated, double lat, double lon, String parentId,float mortalityIncrementPercentage,
                float confirmedIncrementPercentage, float recoveredIncrementPercentage) {
        this.id = id;
        this.displayName = displayName;
        this.areas = areas;
        this.totalConfirmed = totalConfirmed;
        this.totalDeaths = totalDeaths;
        this.totalRecovered = totalRecovered;
        this.totalRecoveredDelta = totalRecoveredDelta;
        this.totalDeathsDelta = totalDeathsDelta;
        this.totalConfirmedDelta = totalConfirmedDelta;
        this.lastUpdated = lastUpdated;
        this.lat = lat;
        this.lon = lon;
        this.parentId = parentId;

        this.mortalityIncrementPercentage = mortalityIncrementPercentage;
        this.confirmedIncrementPercentage = confirmedIncrementPercentage;
        this.recoveredIncrementPercentage = recoveredIncrementPercentage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public int getTotalConfirmed() {
        return totalConfirmed;
    }

    public void setTotalConfirmed(int totalConfirmed) {
        this.totalConfirmed = totalConfirmed;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public void setTotalDeaths(int totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }

    public void setTotalRecovered(int totalRecovered) {
        this.totalRecovered = totalRecovered;
    }

    public int getTotalDeathsDelta() {
        return totalDeathsDelta;
    }

    public void setTotalDeathsDelta(int totalDeathsDelta) {
        this.totalDeathsDelta = totalDeathsDelta;
    }

    public int getTotalConfirmedDelta() {
        return totalConfirmedDelta;
    }

    public void setTotalConfirmedDelta(int totalConfirmedDelta) {
        this.totalConfirmedDelta = totalConfirmedDelta;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getTotalRecoveredDelta() {
        return totalRecoveredDelta;
    }

    public void setTotalRecoveredDelta(int totalRecoveredDelta) {
        this.totalRecoveredDelta = totalRecoveredDelta;
    }

    public float getMortalityIncrementPercentage() {
        return mortalityIncrementPercentage;
    }

    public void setMortalityIncrementPercentage(float mortalityIncrementPercentage) {
        this.mortalityIncrementPercentage = mortalityIncrementPercentage;
    }

    public float getConfirmedIncrementPercentage() {
        return confirmedIncrementPercentage;
    }

    public void setConfirmedIncrementPercentage(float confirmedIncrementPercentage) {
        this.confirmedIncrementPercentage = confirmedIncrementPercentage;
    }

    public float getRecoveredIncrementPercentage() {
        return recoveredIncrementPercentage;
    }

    public void setRecoveredIncrementPercentage(float recoveredIncrementPercentage) {
        this.recoveredIncrementPercentage = recoveredIncrementPercentage;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public float getMortalityRate() {
        return mortalityRate;
    }

    public void setMortalityRate(float mortalityRate) {
        this.mortalityRate = mortalityRate;
    }

    public float getRecoveredRate() {
        return recoveredRate;
    }

    public void setRecoveredRate(float recoveredRate) {
        this.recoveredRate = recoveredRate;
    }

    public void setPercentages(){
        try {
            if (this.totalDeaths - this.totalDeathsDelta != 0)
                this.mortalityIncrementPercentage = (((float) this.totalDeaths * 100) / ((float) this.totalDeaths - (float) this.totalDeathsDelta)) - 100;
            else
                this.mortalityIncrementPercentage = 0;
            if (this.totalConfirmed - this.totalConfirmedDelta != 0)
                this.confirmedIncrementPercentage = ((float) (this.totalConfirmed * 100) / ((float) this.totalConfirmed - (float) this.totalConfirmedDelta)) - 100;
            else
                this.confirmedIncrementPercentage = 0;
            if (this.totalRecovered - this.totalRecoveredDelta != 0)
                this.recoveredIncrementPercentage = ((float) (this.totalRecovered * 100) / ((float) this.totalRecovered - (float) this.totalRecoveredDelta)) - 100;
            else
                this.recoveredIncrementPercentage = 0;
            if (this.totalConfirmed != 0)
                this.mortalityRate = ((float) (this.totalDeaths * 100) / ((float) this.totalConfirmed));
            else
                this.mortalityRate = 0;
            if (this.totalConfirmed != 0)
                this.recoveredRate = ((float) (this.totalRecovered * 100) / ((float) this.totalConfirmed));
            else
                this.recoveredRate = 0;
        }catch(ArithmeticException e){
            e.getMessage();
        }
    }
}
