package com.softtek.covid.covidstatistics;

import com.softtek.covid.covidstatistics.controller.DatasController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CovidDatasApplicationTests {

	@Autowired
	private DatasController controller;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
