package com.softtek.covid.covidstatistics;

import com.softtek.covid.covidstatistics.model.entity.Area;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@WebMvcTest
public class DatasControllerTest {

    @Mock
    Area area;

    @Autowired
    MockMvc mockMvc;

    @Test
    void testPercentagesCalculator(){
        //Given. With values that may cause division by zero if method have a problem
        area.setTotalConfirmed(0);
        area.setTotalDeaths((int)Math.random()*1000);
        area.setTotalRecovered((int)Math.random()*1000);

        area.setTotalDeathsDelta(area.getTotalDeaths());
        area.setTotalRecoveredDelta(area.getTotalRecovered());
        area.setTotalConfirmedDelta(area.getTotalConfirmed());

        area.setPercentages();

        //asserts to make sure that division by zero won't happen
        assertThat(area.getMortalityRate()).isEqualTo(0);
        assertThat(area.getRecoveredRate()).isEqualTo(0);

        assertThat(area.getMortalityIncrementPercentage()).isEqualTo(0);
        assertThat(area.getConfirmedIncrementPercentage()).isEqualTo(0);
        assertThat(area.getRecoveredIncrementPercentage()).isEqualTo(0);
    }

    @Test
    void shouldGetGlobalData(){
        try {
            MvcResult mvcResult = mockMvc.perform(get("/datas")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();

            String result = mvcResult.getResponse().getContentAsString();
            assertNotNull(result);
            assertTrue(result.contains("\"displayName\":\""+"Global"+"\""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldGetSpecificCountryData(){
        try {
            String countryToTest = "México";
            String countryId = "mexico";
            MvcResult mvcResult = mockMvc.perform(get("/datas?country="+countryToTest)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            String result = mvcResult.getResponse().getContentAsString();
            assertNotNull(result);
            assertTrue(result.contains("\"id\":\""+countryId+"\""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldGetSpecificStateData(){
        try {
            String countryToTest = "México";
            String parentId = "mexico";
            String stateToTest = "Aguascalientes";
            MvcResult mvcResult = mockMvc.perform(get("/datas?country="+countryToTest+"&state="+stateToTest)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();

            String result = mvcResult.getResponse().getContentAsString();
            assertNotNull(result);
            assertTrue(result.contains("\"parentId\":\""+parentId+"\""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void shouldGetNull(){
        try {
            String erroneousCountryString = "mejiko";
            String erroneousStateString = "duranjo";

            //Verify that when country param is wrong and state is null, status returned is ok and result is an empty array
            MvcResult mvcResult = mockMvc.perform(get("/datas?country="+erroneousCountryString)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNoContent())
                    .andReturn();

            String result = mvcResult.getResponse().getContentAsString();
            assertTrue(result.equals("[]"));

            //Verify that when country param is correct and state param is wrong, status returned is ok and result is an empty array
            mvcResult = mockMvc.perform(get("/datas?country=México&state="+erroneousStateString)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNoContent())
                    .andReturn();

            result = mvcResult.getResponse().getContentAsString();
            assertTrue(result.equals("[]"));

            //Verify that when country param and state param are wrong, status returned is ok and result is an empty array
            mvcResult = mockMvc.perform(get("/datas?country="+erroneousCountryString+"&state="+erroneousStateString)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNoContent())
                    .andReturn();

            result = mvcResult.getResponse().getContentAsString();
            assertTrue(result.equals("[]"));

            //Verify that when country param is wrong and state param is correct, status returned is ok and result is an empty array
            mvcResult = mockMvc.perform(get("/datas?country="+erroneousCountryString+"&state=Sonora")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNoContent())
                    .andReturn();

            result = mvcResult.getResponse().getContentAsString();
            assertTrue(result.equals("[]"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
